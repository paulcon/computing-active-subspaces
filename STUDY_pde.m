%%
% Compute the active subspace for the PDE problem

clear all; close all

% Get the PDE geometry, mesh, and boundary data
pde_data = get_pde_data();

% Get KL bases
corr_length = 1; % correlation length for PDE random coefficients

% Load the initial random study
T=load('gp/testing0.mat'); X = T.X; clear T;
% M is number of samples
% m is dimension of space
[M,m] = size(X);

% Get the PDE solutions and gradients
if corr_length == 1
    filename='long_corr.mat';
    savename='long_corr';
elseif corr_length == 0.01
    filename='short_corr.mat';
    savename='short_corr';
else
    filename=sprintf('%0.10d.mat',randi(1e9));
end
[U,~] = get_kl_bases(corr_length,m,pde_data,filename);
[f,G] = get_pde_solutions(X,U,pde_data,filename);

%% Dimension of subspace
% number of eigenvalues to test.
n = 6; 

% multiplier
alpha = 10; 

% number of samples
Nmin = ceil(alpha*n*log(m));

% eigenvalues
GG = G(1:Nmin,:);
sigs = svd(GG,'econ');
lambda_hat = (1/Nmin)*sigs(1:n).^2;

% bootstrap for confidence intervals
n_boot = 10000;
ind_boot = randi(Nmin,Nmin,n_boot);
evals_boot = zeros(n,n_boot);
for i=1:n_boot
    ind = ind_boot(:,i);
    sigs = svd(GG(ind,:));
    evals_boot(:,i) = (1/Nmin)*sigs(1:n).^2;
end

lambda_hat_upper = zeros(n,1); 
lambda_hat_lower = zeros(n,1);
for i=1:n
    e = sort(evals_boot(i,:),'ascend');
    lambda_hat_upper(i) = e(ceil(0.995*n_boot));
    lambda_hat_lower(i) = e(floor(0.005*n_boot));
end

% eigenvectors
[W,~,~] = svd(GG','econ');

% bootstrap for confidence intervals
n_boot = 10000;
ind_boot = randi(Nmin,Nmin,n_boot);
se_boot = zeros(n,n_boot);
for i=1:n_boot
    ind = ind_boot(:,i);
    [W0,~,~] = svd(GG(ind,:)','econ');
    se = zeros(n,1);
    for j=1:n
        se(j) = norm(W0(:,1:j)'*W(:,j+1:end));
    end
    se_boot(:,i) = se;
end

subspace_err_upper = zeros(n,1); 
subspace_err_lower = zeros(n,1);
for i=1:n
    e = sort(se_boot(i,:),'ascend');
    subspace_err_upper(i) = e(ceil(0.995*n_boot));
    subspace_err_lower(i) = e(floor(0.005*n_boot));
end
subspace_err_mean = mean(se_boot,2);

save(sprintf('pdestudy_mult_%0.2d_%s.mat',alpha,savename),...
    'subspace_err_upper','subspace_err_lower',...
    'subspace_err_mean','lambda_hat',...
    'lambda_hat_upper','lambda_hat_lower');

