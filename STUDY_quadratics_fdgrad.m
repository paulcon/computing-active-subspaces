%%
% Test with quadratics for computing active subspace
% f(x) = 0.5*x'*A*x, A = A'
% df(x) = A*x
% C = E[A*x*x'*A'] = (1/3)*A^2

clear all; close all;
% loads random samples X and random orthgonal Q
load('samples.mat');
[m,M] = size(X);

%%
a1 = [-2; 0.5*(2:m)']; 
a2 = [0.5*(-4:-2)'; 0.5*(4:m)']; 
a3 = 0.5*(-4:5)'; 

evalz = cell(3,1);
evalz{1} = 10.^(-a1);
evalz{2} = 10.^(-a2);
evalz{3} = 10.^(-a3);

%%
figure(1);

semilogy(1:m,evalz{1},'bo',1:m,evalz{2},'rx',1:m,evalz{3},'gs',...
    'LineWidth',2,'MarkerSize',14);
set(gca,'FontSize',14);
axis square; grid on;
xlabel('Index');
ylabel('Eigenvalues');

%%
hz = [0.01 0.005 0.001 0.0005 0.0001 0.00005 0.00001];

lambda_hat = cell(length(hz),3);
lambda_hat_upper = cell(length(hz),3);
lambda_hat_lower = cell(length(hz),3);

% studying eigenvalues
Az = cell(3,1);
Az{1} = Q*(diag(evalz{1})*Q');
Az{2} = Q*(diag(evalz{2})*Q');
Az{3} = Q*(diag(evalz{3})*Q');

% number of eigenvalues to test.
n = 6; 

% multiplier
alpha = 2; 

% number of samples
Nmin = ceil(alpha*n*log(m));

for l = 1:length(hz)
    tic;
    for k = 1:3

        % eigenvalues
        XX = X(:,1:Nmin);
        G = fd_grad(Az{k},XX,hz(l));
        sigs = svd(G);
        lambda_hat{l,k} = (1/Nmin)*sigs.^2;

        % bootstrap for confidence intervals
        n_boot = 10000;
        ind_boot = randi(Nmin,Nmin,n_boot);
        evals_boot = zeros(n,n_boot);
        for i=1:n_boot
            ind = ind_boot(:,i);
            sigs = svd(G(:,ind));
            evals_boot(:,i) = (1/Nmin)*sigs(1:n).^2;
        end

        upper_boot = zeros(n,1); lower_boot = zeros(n,1);
        for i=1:n
            e = sort(evals_boot(i,:),'ascend');
            upper_boot(i) = e(ceil(0.995*n_boot));
            lower_boot(i) = e(floor(0.005*n_boot));
        end
        
        lambda_hat_upper{l,k} = upper_boot;
        lambda_hat_lower{l,k} = lower_boot;
        
    end
    fprintf('h = %4.2e, time %4.2f sec\n',hz(l),toc);
end

%%
for l = 1:length(hz)
    for k = 1:3
        %
        figure(2);
        semilogy(1:n,(1/3)*evalz{k}(1:n).^2,'bo',...
            1:n,lambda_hat{l,k}(1:n),'rx',...
            1:n,lambda_hat_lower{l,k},'g^',...
            1:n,lambda_hat_upper{l,k},'gv',...
            'MarkerSize',14,'LineWidth',2);
        axis square; grid on;
        set(gca,'FontSize',14);
        legend('true','est','CI','Location','NorthEast');
        ylim([1e-8 1e4]);
        print(sprintf('figs/evals_A%0.2d_h%0.2d',k,l),'-depsc2','-r300');
        title(sprintf('h = %4.2e',hz(l)));
        pause;

    end
end


%%
% studying eigenvectors
subspace_errors = cell(length(hz),3);

for l=1:length(hz)
    tic
    for k=1:3

        Nmax = 6;
        NN = 10.^(1:Nmax);
        s_err = zeros(Nmax,n);
        G = fd_grad(Az{k},X,hz(l));
        for i=1:Nmax
            [~,~,W] = svd(G(:,1:NN(i))','econ');
            for j=1:n
                s_err(i,j) = norm(W(:,1:j)'*Q(:,j+1:end));
            end
        end
        
        subspace_errors{l,k} = s_err;

    end
    
    fprintf('h = %4.2e, time %4.2f sec\n',hz(l),toc);
end
    
    
%%
% studying eigenvectors
for k=1:3
    for l=1:length(hz)


        %
        figure(3);
        loglog(NN,NN.^(-0.5),'k-',...
            NN,subspace_errors{l,k}(:,1),'ro-',...
            NN,subspace_errors{l,k}(:,2),'bo-',...
            NN,subspace_errors{l,k}(:,3),'go-',...
            NN,subspace_errors{l,k}(:,4),'mo-',...
            NN,subspace_errors{l,k}(:,5),'co-',...
            NN,subspace_errors{l,k}(:,6),'yo-',...
            'MarkerSize',14,'LineWidth',2);
        axis square; grid on;
        set(gca,'FontSize',14);
        xlabel('N');
        ylabel('Subspace distance');
        legend('N^{-1/2}','1','2','3','4','5','6','Location','NorthEast');
        ylim([1e-6 1]); xlim([10 1e6]);
        print(sprintf('figs/subspace_err_A%0.2d_h%0.2d',k,l),'-depsc','-r300');
        title(sprintf('h = %4.2e',hz(l)));
        pause;

    end

end










































