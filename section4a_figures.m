%%
close all; clear all;
load('quadstudy_mult_02');
n = size(lambda_hat{1,1},1);
hz = [0.1 0.001 0.00001];

%%
axisfs = 10;
markersize = 8;
linewidth = 1;

%%

figure(1);
for k=1:3
    clf;
    semilogy(1:length(evalz{k}),evalz{k},'bo-',...
        'LineWidth',linewidth,'MarkerSize',markersize);
    set(gca,'FontSize',axisfs);
    axis square; grid on;
    ylim([1e-6 1e2]); xlim([1 10]);
    xlabel('Index'); ylabel('Eigenvalues');
    
    set(gca,'YTick',[1e-6,1e-4,1e-2,1,1e2]);
    set_figure_size([3,3]); greygrid(gca,0.55*[1,1,1]);
    
    print(sprintf('figs/Aeigs_%0.2d',k),'-depsc2','-painters');
end

%% plot eigenvalues 

for k=1:3
    figure; 
    hl = semilogy(1:n,(1/3)*evalz{k}(1:n).^2,'bo-',...
        1:n,lambda_hat{1,k},'rx-',...
        'LineWidth',linewidth,'MarkerSize',markersize);
    axis square; grid on;
    set(gca,'FontSize',axisfs);
    ylim([1e-8 1e4]);
    xlabel('Index'); ylabel('Eigenvalues');
    hold on;
    hp = patch([(1:n)';flipud((1:n)')],...
        [lambda_hat_upper{1,k}; flipud(lambda_hat_lower{1,k})],...
        [0.7 0.7 0.7],'EdgeColor',[0.7 0.7 0.7]);
    uistack(hp,'bottom');
    legend([hl;hp],'True','Est','CI','Location','NorthEast');
    hold off;
    set(gca,'YTick',[1e-8,1e-6,1e-4,1e-2,1,1e2,1e4]);
    set(gca,'XTick',1:6); xlim([1,6]);
    set_figure_size([3,3]); greygrid(gca,0.55*[1,1,1]);
    print(sprintf('figs/evals_A%0.2d',k),'-depsc2','-painters');
end

%%

for l = 2:length(hz)+1
    for k = 1:3
        %
        figure;
        hl = semilogy(1:n,(1/3)*evalz{k}(1:n).^2,'bo-',...
            1:n,lambda_hat{l,k}(1:n),'rx-',...
            [1 n],[hz(l-1) hz(l-1)],'k-',...
            'MarkerSize',markersize,'LineWidth',linewidth);
        axis square; grid on;
        set(gca,'FontSize',axisfs);
        ylim([1e-8 1e4]);
        xlabel('Index'); ylabel('Eigenvalues');
        hold on;
        hp = patch([(1:n)';flipud((1:n)')],...
            [lambda_hat_upper{l,k}; flipud(lambda_hat_lower{l,k})],...
            [0.7 0.7 0.7],'EdgeColor',[0.7 0.7 0.7]);
        uistack(hp,'bottom');
        %legend('True','Est','CI','Location','NorthEast');
        
        set(hl(3),'LineWidth',2*linewidth,'Color','k');
        hold off;
        
        
        set(gca,'YTick',[1e-8,1e-6,1e-4,1e-2,1,1e2,1e4]);
        set(gca,'XTick',1:6); xlim([1,6]);
        set_figure_size([3,3]); greygrid(gca,0.55*[1,1,1]);
        hl = legend([hl(1:2);hp],'True','Est','CI','Location','NorthEast');
        
        print(sprintf('figs/evals_A%0.2d_h%0.2d',k,l),'-depsc2','-painters');
        %title(sprintf('h = %4.2e',hz(l)));
        

    end
end


%% plot eigenvectors

for k=1:3
    figure;
    hl = semilogy(1:n,subspace_err{1,k},'bo-',...
        1:n,subspace_err_mean{1,k},'rx-',...
        'MarkerSize',markersize,'LineWidth',linewidth);
    axis square; grid on;
    set(gca,'FontSize',axisfs);
    ylim([1e-6 2]);
    xlabel('Subspace Dimension'); ylabel('Distance');
    hold on;
    hp = patch([(1:n)';flipud((1:n)')],...
        [subspace_err_upper{1,k}; flipud(subspace_err_lower{1,k})],...
        [0.7 0.7 0.7],'EdgeColor',[0.7 0.7 0.7]);
    uistack(hp,'bottom');
    legend([hl;hp],'True','Est','CI','Location','SouthEast');
    hold off;
    
    xlim([1,6]);
    set(gca,'YTick',[1e-6,1e-4,1e-2,1]);
    set_figure_size([3,3]); greygrid(gca,0.55*[1,1,1]);
    
    print(sprintf('figs/subspace_A%0.2d',k),'-depsc2','-r300');
    
end

%%

for l = 2:length(hz)+1
    for k = 1:3
        %
        clf;
        hl = semilogy(1:n,subspace_err{l,k}(1:n),'bo-',...
            1:n,subspace_err_mean{l,k}(1:n),'rx-',...
            [1 n],[hz(l-1) hz(l-1)],'k-',...
            'MarkerSize',markersize,'LineWidth',linewidth);
        axis square; grid on;
        set(gca,'FontSize',axisfs);
        %legend('True','Est','CI','Location','SouthEast');
        ylim([1e-6 2]); 
        xlabel('Subspace Dimension'); ylabel('Distance');
        hold on;
        hp = patch([(1:n)';flipud((1:n)')],...
            [subspace_err_upper{l,k}; flipud(subspace_err_lower{l,k})],...
            [0.7 0.7 0.7],'EdgeColor',[0.7 0.7 0.7]);
        uistack(hp,'bottom');
       
        set(hl(3),'LineWidth',2*linewidth,'Color','k');
        hold off;
        
        xlim([1,6]);
        set(gca,'YTick',[1e-6,1e-4,1e-2,1]);
        set_figure_size([3,3]); greygrid(gca,0.55*[1,1,1]);
        hl = legend([hl(1:2);hp],'True','Est','CI','Location','SouthEast');
        
        print(sprintf('figs/subspace_A%0.2d_h%0.2d',k,l),'-depsc2','-painters');
        %title(sprintf('h = %4.2e',hz(l)));
        

    end
end