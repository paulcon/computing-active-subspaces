%%
% Test with quadratics for computing active subspace
% f(x) = 0.5*x'*A*x, A = A'
% df(x) = A*x
% C = E[A*x*x'*A'] = (1/3)*A^2
%
% So here are the things we'll do for the test.
% (0) Eig of A: 1,2,3
% (1) Eig estimates with boostrap CIs, A: 1,2,3
% (2) Eig estimates with fd and bootstrap CIs, 
%   A: 1,2,3, h: 0.1,0.01,0.001
% (3) Subspace distance with bootstrap CIs, A: 1,2,3
% (4) Subspace distance with fd and bootstrap CIs, 
%   A: 1,2,3, h: 0.1,0.01,0.001
%
% And for each multiplier?

clear all; close all;
% loads random samples X and random orthgonal Q
load('quad_samples.mat');
[m,M] = size(X);

%%
a1 = [-2; 0.5*(2:m)']; 
a2 = [0.5*(-4:-2)'; 0.5*(4:m)']; 
a3 = 0.5*(-4:5)'; 

evalz = cell(3,1);
evalz{1} = 10.^(-a1);
evalz{2} = 10.^(-a2);
evalz{3} = 10.^(-a3);


% studying eigenvalues
Az = cell(3,1);
Az{1} = Q*(diag(evalz{1})*Q');
Az{2} = Q*(diag(evalz{2})*Q');
Az{3} = Q*(diag(evalz{3})*Q');

% number of eigenvalues to test.
n = 6; 

% multiplier
alpha = 2; 

% number of samples
Nmin = ceil(alpha*n*log(m));

%%
% eigenvalues
hz = [0.1 0.001 0.00001];

lambda_hat = cell(length(hz)+1,3);
lambda_hat_upper = cell(length(hz)+1,3);
lambda_hat_lower = cell(length(hz)+1,3);

for k = 1:3

    % eigenvalues
    XX = X(:,1:Nmin);
    sigs = svd(Az{k}*XX);
    lambda_hat{1,k} = (1/Nmin)*sigs(1:n).^2;

    % bootstrap for confidence intervals
    n_boot = 10000;
    ind_boot = randi(Nmin,Nmin,n_boot);
    evals_boot = zeros(n,n_boot);
    for i=1:n_boot
        ind = ind_boot(:,i);
        sigs = svd(Az{k}*XX(:,ind));
        evals_boot(:,i) = (1/Nmin)*sigs(1:n).^2;
    end
    
    upper_boot = zeros(n,1); lower_boot = zeros(n,1);
    for i=1:n
        e = sort(evals_boot(i,:),'ascend');
        upper_boot(i) = e(ceil(0.995*n_boot));
        lower_boot(i) = e(floor(0.005*n_boot));
    end
    
    lambda_hat_upper{1,k} = upper_boot;
    lambda_hat_lower{1,k} = lower_boot;
    
end

for l = 2:length(hz)+1
    tic;
    for k = 1:3

        % eigenvalues
        XX = X(:,1:Nmin);
        G = fd_grad(Az{k},XX,hz(l-1));
        sigs = svd(G);
        lambda_hat{l,k} = (1/Nmin)*sigs.^2;

        % bootstrap for confidence intervals
        n_boot = 10000;
        ind_boot = randi(Nmin,Nmin,n_boot);
        evals_boot = zeros(n,n_boot);
        for i=1:n_boot
            ind = ind_boot(:,i);
            sigs = svd(G(:,ind));
            evals_boot(:,i) = (1/Nmin)*sigs(1:n).^2;
        end

        upper_boot = zeros(n,1); lower_boot = zeros(n,1);
        for i=1:n
            e = sort(evals_boot(i,:),'ascend');
            upper_boot(i) = e(ceil(0.995*n_boot));
            lower_boot(i) = e(floor(0.005*n_boot));
        end
        
        lambda_hat_upper{l,k} = upper_boot;
        lambda_hat_lower{l,k} = lower_boot;
        
    end
    fprintf('h = %4.2e, time %4.2f sec\n',hz(l-1),toc);
end


%%
subspace_err = cell(length(hz)+1,3);
subspace_err_mean = cell(length(hz)+1,3);
subspace_err_upper = cell(length(hz)+1,3);
subspace_err_lower = cell(length(hz)+1,3);

for k = 1:3

    % eigenvectors
    XX = X(:,1:Nmin);
    [W,~,~] = svd(Az{k}*XX,'econ');
    se = zeros(n,1);
    for i=1:n
        se(i) = norm(W(:,1:i)'*Q(:,i+1:end));
    end
    subspace_err{1,k} = se;

    % bootstrap for confidence intervals
    n_boot = 10000;
    ind_boot = randi(Nmin,Nmin,n_boot);
    se_boot = zeros(n,n_boot);
    for i=1:n_boot
        ind = ind_boot(:,i);
        [W0,~,~] = svd(Az{k}*XX(:,ind),'econ');
        se = zeros(n,1);
        for j=1:n
            se(j) = norm(W0(:,1:j)'*W(:,j+1:end));
        end
        se_boot(:,i) = se;
    end
    
    upper_boot = zeros(n,1); lower_boot = zeros(n,1);
    for i=1:n
        e = sort(se_boot(i,:),'ascend');
        upper_boot(i) = e(ceil(0.995*n_boot));
        lower_boot(i) = e(floor(0.005*n_boot));
    end
    subspace_err_mean{1,k} = mean(se_boot,2);
    subspace_err_upper{1,k} = upper_boot;
    subspace_err_lower{1,k} = lower_boot;
    
end

for l = 2:length(hz)+1
    tic;

    for k = 1:3

        % eigenvalues
        XX = X(:,1:Nmin);
        G = fd_grad(Az{k},XX,hz(l-1));
        [W,~,~] = svd(G,'econ');
        se = zeros(n,1);
        for i=1:n
            se(i) = norm(W(:,1:i)'*Q(:,i+1:end));
        end
        subspace_err{l,k} = se;

        % bootstrap for confidence intervals
        n_boot = 10000;
        ind_boot = randi(Nmin,Nmin,n_boot);
        se_boot = zeros(n,n_boot);
        for i=1:n_boot
            ind = ind_boot(:,i);
            [W0,~,~] = svd(G(:,ind),'econ');
            se = zeros(n,1);
            for j=1:n
                se(j) = norm(W0(:,1:j)'*W(:,j+1:end));
            end
            se_boot(:,i) = se;
        end

        upper_boot = zeros(n,1); lower_boot = zeros(n,1);
        for i=1:n
            e = sort(se_boot(i,:),'ascend');
            upper_boot(i) = e(ceil(0.995*n_boot));
            lower_boot(i) = e(floor(0.005*n_boot));
        end
        subspace_err_mean{l,k} = mean(se_boot,2);
        subspace_err_upper{l,k} = upper_boot;
        subspace_err_lower{l,k} = lower_boot;

    end
    fprintf('h = %4.2e, time %4.2f sec\n',hz(l-1),toc);
end


%%
save(sprintf('quadstudy_mult_%0.2d.mat',alpha),'Az','evalz',...
    'lambda_hat','lambda_hat_upper','lambda_hat_lower',...
    'subspace_err','subspace_err_mean','subspace_err_upper',...
    'subspace_err_lower');
































