function G = fd_grad(A,X,h)

[m,M] = size(X);
G = zeros(m,M);

AX = A*X;
f0 = 0.5*sum(bsxfun(@times,X,AX),1);

for i=1:m
    e = zeros(m,1); e(i) = h;
    Xp = bsxfun(@plus,X,e);
    AXp = A*Xp;
    fp = 0.5*sum(bsxfun(@times,Xp,AXp),1);
    G(i,:) = (fp-f0)/h;
end

