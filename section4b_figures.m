%%
close all; clear all;
%fname='long_corr'; alpha = 2;
%fname='long_corr'; alpha = 10;
%fname='short_corr'; alpha = 2;
fname='short_corr'; alpha = 10;
load(sprintf('pdestudy_mult_%0.2d_%s.mat',alpha,fname));
n = size(lambda_hat,1);

axisfs = 11;
markersize = 8;
linewidth = 1;


%%

% plot eigenvalues 
figure(2);

hl = semilogy(1:n,lambda_hat,'rx-',...
    'MarkerSize',markersize,'LineWidth',linewidth);
axis square; grid on; 
set(gca,'FontSize',axisfs);
ylim([1e-13 1e-6]);
xlabel('Index'); ylabel('Eigenvalues');
hold on;
hp = patch([(1:n)';flipud((1:n)')],...
    [lambda_hat_upper; flipud(lambda_hat_lower)],...
    [0.7 0.7 0.7],'EdgeColor',[0.7 0.7 0.7]);
uistack(hp,'bottom');
legend([hl;hp],'Est','CI','Location','NorthEast');
hold off;

set(gca,'XMinorGrid','off');
set(gca,'YMinorGrid','off');
set(gca,'YTick',[1e-13,1e-12,1e-11,1e-10,1e-9,1e-8,1e-7,1e-6]); xlim([1,6]);
set_figure_size([4,4]); greygrid(gca,0.55*[1,1,1]);

set(gca,'XMinorGrid','off');
set(gca,'YMinorGrid','off');    

print(sprintf('figs/pde_evals_%0.2d_%s',alpha,fname),'-depsc2','-painters');

% plot eigenvectors
figure(4);
hl = semilogy(1:n,subspace_err_mean,'rx-',...
    'MarkerSize',markersize,'LineWidth',linewidth);
axis square; grid on;
set(gca,'FontSize',axisfs);
ylim([1e-2 1.1]);
xlabel('Subspace Dimension'); ylabel('Distance');
hold on;
hp = patch([(1:n)';flipud((1:n)')],...
    [subspace_err_upper; flipud(subspace_err_lower)],...
    [0.7 0.7 0.7],'EdgeColor',[0.7 0.7 0.7]);
uistack(hp,'bottom');
legend('CI','Est','Location','SouthEast');
hold off;

set(gca,'XMinorGrid','off');
set(gca,'YMinorGrid','off');
set(gca,'YTick',[1e-2,1e-1,1]); xlim([1,6]);
set_figure_size([4,4]); greygrid(gca,0.55*[1,1,1]);

set(gca,'XMinorGrid','off');
set(gca,'YMinorGrid','off');    

print(sprintf('figs/pde_subspace_%0.2d_%s',alpha,fname),'-depsc2','-painters');

