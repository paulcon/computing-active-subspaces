%%
close all;
figure;
m = 1000;
plot(1:m,12*log(1:m),'b-',...
    1:m,60*log(1:m),'r-',...
    'LineWidth',2);
axis square; grid on;
set(gca,'FontSize',14);
xlabel('Dimension');
ylabel('N');
legend('\alpha=2','\alpha=10','Location','Best');
print('../figs/Nstrat','-depsc2','-r300');